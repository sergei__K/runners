/**
 * Created by g15oit18 on 06.11.2017.
 */
public class Runners extends Thread {
    private String name;
    private int priority1;
    private int priority2;

    public Runners(String name, int priority1, int priority2) {
        this.name = name;
        this.priority1 = priority1;
        this.priority2 = priority2;
    }

    public void run() {
        for (int i = 0; i < 1000; i++) {
            if (i==350){
                setPriority(priority1);
            }if (i==650){
                setPriority(priority2);
            }
            try {
                sleep(10);
            } catch (InterruptedException e) {
            }
            System.out.println(name);
        }
    }
}